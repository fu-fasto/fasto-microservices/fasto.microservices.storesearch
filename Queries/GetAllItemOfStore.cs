﻿using System;
using FastO.Microservices.StoreSearch.DataModels;
using MicroBoost.Cqrs.Queries;

namespace FastO.Microservices.StoreSearch.Queries
{
    public class GetAllItemOfStore : AllQueryBase<Item>
    {
        public Guid StoreId { get; set; }
    }
}