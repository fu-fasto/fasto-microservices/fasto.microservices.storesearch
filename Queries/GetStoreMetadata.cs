﻿using System.Collections.Generic;
using FastO.Microservices.StoreSearch.Dtos;
using MicroBoost.Cqrs.Queries;

namespace FastO.Microservices.StoreSearch.Queries
{
    public class GetStoreMetadata : IQuery<StoreMetadata>, IStoreQueryBase
    {
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public IList<int> Cuisines { get; set; }
        public IList<int> Categories { get; set; }
        
        public string Text { get; set; }
    }
}