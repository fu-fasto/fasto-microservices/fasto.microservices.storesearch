﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.StoreSearch.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Persistence;

namespace FastO.Microservices.StoreSearch.Queries.Handlers
{
    public class GetOneItemHandler : IQueryHandler<GetOneItem, Item>
    {
        private readonly IRepository<Item, Guid> _itemRepository;

        public GetOneItemHandler(IRepository<Item, Guid> itemRepository)
        {
            _itemRepository = itemRepository;
        }

        public async Task<Item> Handle(GetOneItem query, CancellationToken cancellationToken)
        {
            return await _itemRepository.FindOneAsync(query.Id, cancellationToken)
                   ?? throw new NullReferenceException($"Item {query.Id} does not exist!");
        }
    }
}