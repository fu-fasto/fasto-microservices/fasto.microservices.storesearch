﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.StoreSearch.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Persistence;

namespace FastO.Microservices.StoreSearch.Queries.Handlers
{
    public class GetOneStoreHandler : IQueryHandler<GetOneStore, Store>
    {
        private readonly IRepository<Store, Guid> _storeRepository;

        public GetOneStoreHandler(IRepository<Store, Guid> storeRepository)
        {
            _storeRepository = storeRepository;
        }

        public async Task<Store> Handle(GetOneStore query, CancellationToken cancellationToken)
        {
            return await _storeRepository.FindOneAsync(query.Id, cancellationToken)
                   ?? throw new NullReferenceException($"Store {query.Id} does not exist!");
        }
    }
}