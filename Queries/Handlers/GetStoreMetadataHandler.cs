﻿using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.StoreSearch.DataModels;
using FastO.Microservices.StoreSearch.Dtos;
using FastO.Microservices.StoreSearch.Services;
using MicroBoost.Cqrs.Queries;

namespace FastO.Microservices.StoreSearch.Queries.Handlers
{
    public class GetStoreMetadataHandler : IQueryHandler<GetStoreMetadata, StoreMetadata>
    {
        private readonly IStoreSearchRepository _storeSearchRepository;

        public GetStoreMetadataHandler(IStoreSearchRepository storeSearchRepository)
        {
            _storeSearchRepository = storeSearchRepository;
        }

        public Task<StoreMetadata> Handle(GetStoreMetadata query, CancellationToken cancellationToken)
        {
            return _storeSearchRepository.GetStoreMetadata(query, cancellationToken);
        }
    }
}