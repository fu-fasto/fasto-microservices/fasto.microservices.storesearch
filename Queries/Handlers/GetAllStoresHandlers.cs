﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.StoreSearch.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Helpers;
using MicroBoost.Persistence;

namespace FastO.Microservices.StoreSearch.Queries.Handlers
{
    public class GetAllStoresHandlers : IQueryHandler<GetAllStores, IEnumerable<Store>>
    {
        private readonly IRepository<Store, Guid> _storeRepository;

        public GetAllStoresHandlers(IRepository<Store, Guid> storeRepository)
        {
            _storeRepository = storeRepository;
        }

        public Task<IEnumerable<Store>> Handle(GetAllStores query, CancellationToken cancellationToken)
        {
            var findQuery = query.MapTo<FindQuery<Store>>();

            return _storeRepository.FindAsync(findQuery, cancellationToken);
        }
    }
}