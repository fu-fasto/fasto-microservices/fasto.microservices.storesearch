﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.StoreSearch.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Persistence;

namespace FastO.Microservices.StoreSearch.Queries.Handlers
{
    public class GetAllItemOfStoreHandler : IQueryHandler<GetAllItemOfStore, IEnumerable<Item>>
    {
        private readonly IRepository<Item, Guid> _itemRepository;

        public GetAllItemOfStoreHandler(IRepository<Item, Guid> itemRepository)
        {
            _itemRepository = itemRepository;
        }

        public async Task<IEnumerable<Item>> Handle(GetAllItemOfStore query, CancellationToken cancellationToken)
        {
            var findQuery = new FindQuery<Item>
            {
                Size = 200
            };
            findQuery.Filters.Add(x => x.StoreId.Equals(query.StoreId));
            
            return await _itemRepository.FindAsync(findQuery, cancellationToken);
        }
    }
}