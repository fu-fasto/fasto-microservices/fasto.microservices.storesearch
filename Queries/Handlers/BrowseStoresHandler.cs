﻿using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.StoreSearch.DataModels;
using FastO.Microservices.StoreSearch.Services;
using MicroBoost.Cqrs.Queries;

namespace FastO.Microservices.StoreSearch.Queries.Handlers
{
    public class BrowseStoresHandler : IQueryHandler<BrowseStores, PagedResult<Store>>
    {
        private readonly IStoreSearchRepository _storeSearchRepository;

        public BrowseStoresHandler(IStoreSearchRepository storeSearchRepository)
        {
            _storeSearchRepository = storeSearchRepository;
        }

        public Task<PagedResult<Store>> Handle(BrowseStores query, CancellationToken cancellationToken)
        {
            return _storeSearchRepository.BrowseStores(query, cancellationToken);
        }
    }
}