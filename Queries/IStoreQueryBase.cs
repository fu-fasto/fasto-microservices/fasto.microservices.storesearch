﻿using System.Collections.Generic;

namespace FastO.Microservices.StoreSearch.Queries
{
    public interface IStoreQueryBase
    {
        int? CityId { get; set; }
        int? DistrictId { get; set; }
        IList<int> Cuisines { get; set; }
        IList<int> Categories { get; set; }
        string Text { get; set; }
    }
}