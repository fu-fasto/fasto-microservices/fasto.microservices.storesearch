﻿using System;
using System.Collections.Generic;
using FastO.Microservices.StoreSearch.DataModels;
using MicroBoost.Cqrs.Queries;

namespace FastO.Microservices.StoreSearch.Queries
{
    public class GetAllStores : AllQueryBase<Store>, IStoreQueryBase
    {
        public int? CityId { get; set; }
        public int? DistrictId { get; set; }
        public IList<int> Categories { get; set; }
        public IList<int> Cuisines { get; set; }
    }
}