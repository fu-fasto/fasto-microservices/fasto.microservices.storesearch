﻿using System;
using MicroBoost.Cqrs.Commands;

namespace FastO.Microservices.StoreSearch.Command
{
    public class DeleteOneItem : CommandBase
    {
        public Guid Id { get; set; }
    }
}