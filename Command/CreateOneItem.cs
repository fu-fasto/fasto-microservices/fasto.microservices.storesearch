﻿using System;
using MicroBoost.Cqrs.Commands;

namespace FastO.Microservices.StoreSearch.Command
{
    public class CreateOneItem : CommandBase
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        
        public string Name { get; set; }
        
        public decimal Price { get; set; }
        
        public string Description { get; set; }

        public string ImagesUrl { get; set; }
        
        public Guid StoreId { get; set; }
    }
}