﻿using System;
using MicroBoost.Cqrs.Commands;

namespace FastO.Microservices.StoreSearch.Command
{
    public class UpdateItem : CommandBase
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
        
        public decimal Price { get; set; }
        
        public string Description { get; set; }

        public string ImageUrl { get; set; }
    }
}