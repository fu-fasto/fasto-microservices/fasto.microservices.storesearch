﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.StoreSearch.DataModels;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Persistence;

namespace FastO.Microservices.StoreSearch.Command.Handlers
{
    public class UpdateItemHandler : ICommandHandler<UpdateItem>
    {
        private readonly IRepository<Item, Guid> _itemRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateItemHandler(IRepository<Item, Guid> itemRepository, IUnitOfWork unitOfWork)
        {
            _itemRepository = itemRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task Handle(UpdateItem command, CancellationToken cancellationToken)
        {
            var item = await _itemRepository.FindOneAsync(command.Id, cancellationToken);
            item.Name = command.Name;
            item.Description = command.Description;
            item.Price = command.Price;
            item.ImageUrl = command.ImageUrl;
            
            using var trans = await _unitOfWork.BeginTransactionAsync(cancellationToken);
            try
            {
                await _itemRepository.UpdateAsync(item, cancellationToken);
                await trans.CommitAsync(cancellationToken); }
            catch (Exception)
            {
                await trans.RollbackAsync(cancellationToken);
                throw;
            }
        }
    }
}