﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.StoreSearch.DataModels;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Helpers;
using MicroBoost.Persistence;

namespace FastO.Microservices.StoreSearch.Command.Handlers
{
    public class CreateOneItemHandler : ICommandHandler<CreateOneItem>
    {
        private readonly IRepository<Item, Guid> _itemRepository;

        public CreateOneItemHandler(IRepository<Item, Guid> itemRepository)
        {
            _itemRepository = itemRepository;
        }

        public async Task Handle(CreateOneItem command, CancellationToken cancellationToken)
        {
            var item = command.MapTo<Item>();
            await _itemRepository.AddAsync(item, cancellationToken);
        }
    }
}