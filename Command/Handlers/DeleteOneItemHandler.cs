﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.StoreSearch.DataModels;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Persistence;

namespace FastO.Microservices.StoreSearch.Command.Handlers
{
    public class DeleteOneItemHandler : ICommandHandler<DeleteOneItem>
    {
        private IRepository<Item, Guid> _itemRepository;

        public DeleteOneItemHandler(IRepository<Item, Guid> itemRepository)
        {
            _itemRepository = itemRepository;
        }

        public async Task Handle(DeleteOneItem command, CancellationToken cancellationToken)
        {
            await _itemRepository.DeleteOneAsync(command.Id, cancellationToken);
        }
    }
}