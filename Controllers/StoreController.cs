﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.StoreSearch.Queries;
using MicroBoost.Cqrs.Queries;
using Microsoft.AspNetCore.Mvc;

namespace FastO.Microservices.StoreSearch.Controllers
{
    [ApiController]
    [Route("stores")]
    public class StoreController : ControllerBase
    {
        private readonly IQueryBus _queryBus;

        public StoreController(IQueryBus queryBus)
        {
            _queryBus = queryBus;
        }

        [HttpGet("browse")]
        public async Task<IActionResult> BrowseStores([FromQuery] BrowseStores query,
            CancellationToken cancellationToken)
        {
            var result = await _queryBus.SendAsync(query, cancellationToken);
            return Ok(result);
        }
        
        [HttpGet("metadata")]
        public async Task<IActionResult> GetStoreMetadata([FromQuery] GetStoreMetadata query,
            CancellationToken cancellationToken)
        {
            var result = await _queryBus.SendAsync(query, cancellationToken);
            return Ok(result);
        }
        
        [HttpGet]
        public async Task<IActionResult> GetAllStores([FromQuery] GetAllStores query,
            CancellationToken cancellationToken)
        {
            var result = await _queryBus.SendAsync(query, cancellationToken);
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetOneStore([FromRoute] Guid id, CancellationToken cancellationToken)
        {
            var result = await _queryBus.SendAsync(new GetOneStore() {Id = id}, cancellationToken);
            return Ok(result);
        }
    }
}