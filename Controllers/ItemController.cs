﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.StoreSearch.Command;
using FastO.Microservices.StoreSearch.Queries;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Cqrs.Queries;
using Microsoft.AspNetCore.Mvc;

namespace FastO.Microservices.StoreSearch.Controllers
{
    [ApiController]
    [Route("items")]
    public class ItemController : Controller
    {
        private readonly IQueryBus _queryBus;
        private readonly ICommandBus _commandBus;

        public ItemController(IQueryBus queryBus, ICommandBus commandBus)
        {
            _queryBus = queryBus;
            _commandBus = commandBus;
        }
        
        [HttpGet("{id}")]
        public async Task<IActionResult> GetOneItem([FromRoute] Guid id, CancellationToken cancellationToken)
        {
            var item = await _queryBus.SendAsync(new GetOneItem {Id = id}, cancellationToken);
            return Ok(item);
        }
        
        [HttpGet("")]
        public async Task<IActionResult> GetAllItemOfStore([FromQuery] GetAllItemOfStore query, CancellationToken cancellationToken)
        {
            var items = await _queryBus.SendAsync(query, cancellationToken);
            return Ok(items);
        }

        [HttpPost]
        public async Task<IActionResult> CreateOneItem([FromBody] CreateOneItem command,
            CancellationToken cancellationToken)
        {
            await _commandBus.SendAsync(command, cancellationToken);
            return Accepted(command.Id);
        }
        
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateItem([FromRoute] Guid id, 
            [FromBody] UpdateItem command,
            CancellationToken cancellationToken)
        {
            command.Id = id;
            await _commandBus.SendAsync(command, cancellationToken);
            return Accepted(command.Id);
        }
        
        [HttpDelete]
        public async Task<IActionResult> DeleteItem([FromQuery] DeleteOneItem command,
            CancellationToken cancellationToken)
        {
            await _commandBus.SendAsync(command, cancellationToken);
            return Accepted(command.Id);
        }
    }
}