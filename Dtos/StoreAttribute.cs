﻿namespace FastO.Microservices.StoreSearch.Dtos
{
    public class StoreAttribute
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
    }
}