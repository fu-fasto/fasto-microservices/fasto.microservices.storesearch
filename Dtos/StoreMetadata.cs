﻿using System.Collections.Generic;

namespace FastO.Microservices.StoreSearch.Dtos
{
    public class StoreMetadata
    {
        public IList<StoreAttribute> Cities { get; set; }
        public IList<StoreAttribute> Districts { get; set; }
        public IList<StoreAttribute> Categories { get; set; }
        public IList<StoreAttribute> Cuisines { get; set; }
    }
}