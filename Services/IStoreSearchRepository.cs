﻿using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.StoreSearch.DataModels;
using FastO.Microservices.StoreSearch.Dtos;
using FastO.Microservices.StoreSearch.Queries;
using MicroBoost.Cqrs.Queries;

namespace FastO.Microservices.StoreSearch.Services
{
    public interface IStoreSearchRepository
    {
        Task<PagedResult<Store>> BrowseStores(BrowseStores query, CancellationToken cancellationToken);
        
        Task<StoreMetadata> GetStoreMetadata(GetStoreMetadata query, CancellationToken cancellationToken);
    }
}