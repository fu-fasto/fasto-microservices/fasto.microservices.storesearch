﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.StoreSearch.DataModels;
using FastO.Microservices.StoreSearch.Dtos;
using FastO.Microservices.StoreSearch.Queries;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Helpers;
using MongoDB.Bson;
using MongoDB.Driver;

namespace FastO.Microservices.StoreSearch.Services
{
    public class StoreSearchRepository : IStoreSearchRepository

    {
        private readonly IMongoCollection<Store> _collection;

        public StoreSearchRepository(IMongoDatabase database)
        {
            _collection = database.GetCollection<Store>(nameof(Store));
            _collection = _collection.WithReadPreference(ReadPreference.SecondaryPreferred);
        }


        public async Task<PagedResult<Store>> BrowseStores(BrowseStores query, CancellationToken cancellationToken)
        {
            List<Store> items;
            var findQuery = query.MapTo<FindQuery<Store>>();
            var filterDefinition = GetFilterDefinition(query);
            var sortDefinition = Builders<Store>.Sort.Ascending(x => x.Id);

            if (!String.IsNullOrWhiteSpace(query.Text))
            {
                sortDefinition = Builders<Store>.Sort.MetaTextScore("score");
                var scoreProjection = Builders<Store>.Projection.MetaTextScore("score");
                var storeProjection = Builders<Store>.Projection.As<Store>();

                items = await _collection
                    .Find(filterDefinition)
                    .Project(scoreProjection)
                    .Sort(sortDefinition)
                    .Skip(findQuery!.Page * findQuery!.Size)
                    .Limit(findQuery.Size)
                    .Project(storeProjection)
                    .ToListAsync(cancellationToken);
            }
            else
            {
                items = await _collection
                    .Find(filterDefinition)
                    .Sort(sortDefinition)
                    .Skip(findQuery!.Page * findQuery!.Size)
                    .Limit(findQuery.Size)
                    .ToListAsync(cancellationToken);
            }

            var totals = await _collection.CountDocumentsAsync(filterDefinition, null, cancellationToken);

            return new PagedResult<Store>
            {
                CurrentPage = findQuery.Page,
                Items = items,
                TotalItems = totals,
                TotalPages = (int) totals / findQuery.Size + (totals % findQuery.Size == 0 ? 0 : 1)
            };
        }

        public async Task<StoreMetadata> GetStoreMetadata(GetStoreMetadata query,
            CancellationToken cancellationToken)
        {
            var storeMetadata = new StoreMetadata();
            var filterDefinition = GetFilterDefinition(query);

            var sortBsonDocument = Builders<BsonDocument>.Sort.Descending("count");
            await Task.WhenAll(new List<Task>()
            {
                Task.Run(async () =>
                {
                    storeMetadata.Cities = (await _collection
                            .Aggregate()
                            .Match(filterDefinition)
                            .Group(new BsonDocument
                            {
                                {
                                    "_id", new BsonDocument
                                    {
                                        {"id", "$city_id"},
                                        {"name", "$city"}
                                    }
                                },
                                {"count", new BsonDocument("$sum", 1)}
                            })
                            .Project(new BsonDocument
                            {
                                {"id", "$_id.id"}, {"name", "$_id.name"}, {"count", 1}
                            })
                            .Sort(sortBsonDocument)
                            .ToListAsync(cancellationToken))
                        .ToStoreAttributes();
                }, cancellationToken),
                Task.Run(async () =>
                {
                    storeMetadata.Districts = (await _collection
                            .Aggregate()
                            .Match(filterDefinition)
                            .Group(new BsonDocument
                            {
                                {
                                    "_id", new BsonDocument
                                    {
                                        {"id", "$district_id"},
                                        {"name", "$district"}
                                    }
                                },
                                {"count", new BsonDocument("$sum", 1)}
                            })
                            .Project(new BsonDocument {{"id", "$_id.id"}, {"name", "$_id.name"}, {"count", 1}})
                            .Sort(sortBsonDocument)
                            .ToListAsync(cancellationToken))
                        .ToStoreAttributes();
                }, cancellationToken),
                Task.Run(async () =>
                {
                    storeMetadata.Cuisines = (await _collection
                            .Aggregate()
                            .Match(filterDefinition)
                            .Project(new BsonDocument {{"_id", 0}, {"cuisines", 1}})
                            .Unwind("cuisines")
                            .Group(new BsonDocument
                            {
                                {"_id", "$cuisines"},
                                {"count", new BsonDocument("$sum", 1)}
                            })
                            .Project(new BsonDocument
                            {
                                {"_id", 0}, {"id", "$_id._id"}, {"name", "$_id.name"}, {"count", 1}
                            })
                            .Sort(sortBsonDocument)
                            .ToListAsync(cancellationToken))
                        .ToStoreAttributes();
                }, cancellationToken),
                Task.Run(async () =>
                {
                    storeMetadata.Categories = (await _collection
                            .Aggregate()
                            .Match(filterDefinition)
                            .Project(new BsonDocument {{"_id", 0}, {"categories", 1}})
                            .Unwind("categories")
                            .Group(new BsonDocument
                            {
                                {"_id", "$categories"},
                                {"count", new BsonDocument("$sum", 1)}
                            })
                            .Project(new BsonDocument
                            {
                                {"_id", 0}, {"id", "$_id._id"}, {"name", "$_id.name"}, {"count", 1}
                            })
                            .Sort(sortBsonDocument)
                            .ToListAsync(cancellationToken))
                        .ToStoreAttributes();
                }, cancellationToken),
            });

            return storeMetadata;
        }

        private FilterDefinition<Store> GetFilterDefinition(IStoreQueryBase queryBase)
        {
            var filterDefinition = Builders<Store>.Filter.Empty;

            if (!String.IsNullOrWhiteSpace(queryBase.Text))
            {
                filterDefinition &= Builders<Store>.Filter.Text(queryBase.Text);
            }

            if (queryBase.CityId.HasValue)
            {
                filterDefinition &= Builders<Store>.Filter.Eq(x => x.CityId, queryBase.CityId.Value);
            }

            if (queryBase.DistrictId.HasValue)
            {
                filterDefinition &= Builders<Store>.Filter.Eq(x => x.DistrictId, queryBase.DistrictId.Value);
            }

            if (queryBase.Cuisines is not null && queryBase.Cuisines.Any())
            {
                filterDefinition &= Builders<Store>.Filter.In("cuisines._id", queryBase.Cuisines);
            }

            if (queryBase.Categories is not null && queryBase.Categories.Any())
            {
                filterDefinition &= Builders<Store>.Filter.In("categories._id", queryBase.Categories);
            }

            return filterDefinition;
        }
    }

    static class StoreSearchRepositoryHelper
    {
        public static List<StoreAttribute> ToStoreAttributes(this List<BsonDocument> documents)
        {
            return documents.Select(x => new StoreAttribute()
            {
                Id = x["id"].AsInt32,
                Name = x["name"].AsString,
                Count = x["count"].AsInt32
            }).ToList();
        }
    }
}