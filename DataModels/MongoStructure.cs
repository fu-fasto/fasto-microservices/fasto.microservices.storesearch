﻿using MongoDB.Bson.Serialization;
using MongoDB.Driver;

namespace FastO.Microservices.StoreSearch.DataModels
{
    public class MongoStructure
    {

        public MongoStructure()
        {
            RegisterClassMap();
            Builders<Store>.IndexKeys.Ascending(f => f.Categories[-1].Id);
        }

        private void RegisterClassMap()
        {
            BsonClassMap.RegisterClassMap<Item>(x =>
            {
                x.AutoMap();
                x.GetMemberMap(m => m.Name).SetElementName("name");
                x.GetMemberMap(m => m.Price).SetElementName("price");
                x.GetMemberMap(m => m.Description).SetElementName("description");
                x.GetMemberMap(m => m.ImageUrl).SetElementName("image_url");
                x.GetMemberMap(m => m.StoreId).SetElementName("store_id");
            });

            BsonClassMap.RegisterClassMap<Store>(x =>
            {
                x.AutoMap();
                x.GetMemberMap(m => m.Name).SetElementName("name");
                x.GetMemberMap(m => m.Description).SetElementName("description");
                x.GetMemberMap(m => m.OpenTime).SetElementName("open_time");
                x.GetMemberMap(m => m.CloseTime).SetElementName("close_time");
                x.GetMemberMap(m => m.Address).SetElementName("address");
                x.GetMemberMap(m => m.PhoneNumber).SetElementName("phone_number");
                x.GetMemberMap(m => m.Star).SetElementName("star");
                x.GetMemberMap(m => m.CityId).SetElementName("city_id");
                x.GetMemberMap(m => m.DistrictId).SetElementName("district_id");
                x.GetMemberMap(m => m.City).SetElementName("city");
                x.GetMemberMap(m => m.District).SetElementName("district");
                x.GetMemberMap(m => m.Cuisines).SetElementName("cuisines");
                x.GetMemberMap(m => m.Categories).SetElementName("categories");
                x.GetMemberMap(m => m.ImageUrl).SetElementName("image_url");
                x.GetMemberMap(m => m.Images).SetElementName("images");
            });

            BsonClassMap.RegisterClassMap<Cuisine>(x =>
            {
                x.AutoMap();
                x.GetMemberMap(m => m.Id).SetElementName("id");
                x.GetMemberMap(m => m.Name).SetElementName("name");
            });

            BsonClassMap.RegisterClassMap<Category>(x =>
            {
                x.AutoMap();
                x.GetMemberMap(m => m.Id).SetElementName("id");
                x.GetMemberMap(m => m.Name).SetElementName("name");
            });
        }
    }
}