﻿using System;
using System.Collections.Generic;
using MicroBoost.Persistence.Mongo;
using MicroBoost.Types;

namespace FastO.Microservices.StoreSearch.DataModels
{
    public class Store : EntityBase<Guid>
    {
        /// <summary>
        /// Id of store
        /// </summary>
        public override Guid Id { get; set; } = Guid.NewGuid();

        /// <summary>
        /// Name of store
        /// </summary>
        [FtsIndex]
        public string Name { get; set; }

        /// <summary>
        /// Description about store
        /// </summary>
        public string Description { get; set; }
        
        /// <summary>
        /// Open time
        /// </summary>
        public DateTimeOffset OpenTime { get; set; }

        /// <summary>
        /// Close time
        /// </summary>
        public DateTimeOffset CloseTime { get; set; }

        /// <summary>
        /// Address of store
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Phone number of store
        /// </summary>
        public string PhoneNumber { get; set; }
        
        /// <summary>
        /// Average star of store
        /// </summary>
        public float Star { get; set; }

        /// <summary>
        /// Id of city
        /// </summary>
        [UniqueIndex] public int CityId { get; set; }

        /// <summary>
        /// Id of district
        /// </summary>
        [UniqueIndex] public int DistrictId { get; set; }

        /// <summary>
        /// City
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// District
        /// </summary>
        public string District { get; set; }
        
        /// <summary>
        /// Main image of store
        /// </summary>
        public string ImageUrl { get; set; }
        
        /// <summary>
        /// Cuisines of store
        /// </summary>
        [RawIndex("Cuisines.Id", "cuisines._id")]
        public IList<Cuisine> Cuisines { get; set; }

        /// <summary>
        /// Categories of store
        /// </summary>
        [RawIndex("Categories.Id", "categories._id")]
        public IList<Category> Categories { get; set; }
        
        /// <summary>
        /// List of image of store
        /// </summary>
        public IList<string> Images { get; set; }
    }

    public class Cuisine
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}