﻿using System;
using MicroBoost.Persistence.Mongo;
using MicroBoost.Types;

namespace FastO.Microservices.StoreSearch.DataModels
{
    public class Item: EntityBase<Guid>
    {
        /// <summary>
        /// Id of the item of menu
        /// </summary>
        public override Guid Id { get; set; } = Guid.NewGuid();
        
        /// <summary>
        /// Name of item
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Price of item
        /// </summary>
        public decimal Price { get; set; }
        
        /// <summary>
        /// Description about the item
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Image of the item
        /// </summary>
        public string ImageUrl { get; set; }
        
        /// <summary>
        /// Store Id of the item
        /// </summary>
        [UniqueIndex]
        public Guid StoreId { get; set; }
    }
}