using MicroBoost.Logger;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace FastO.Microservices.StoreSearch
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseLogger().UseStartup<Startup>(); });
    }
}